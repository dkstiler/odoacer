import asyncio
from datetime import datetime

from kivy.app import App
from kivy.lang import Builder
from kivy.config import Config
from kivy.uix.screenmanager import ScreenManager

from kivy.clock import Clock
from kivy.core.clipboard import Clipboard

import os
import json

import valens

Config.set('kivy','log_level','debug')
PORT = 5920


def esc_markup(msg):
    return (msg.replace('&', '&amp;')
            .replace('[', '&bl;')
            .replace(']', '&br;'))


class ClientProtocol(asyncio.Protocol):
    def __init__(self, app, loop):
        self.app = app
        self.loop = loop

    def connection_made(self, transport):
        self.transport = transport

    def data_received(self, data):
        if data:
            text = data.decode('utf-8', 'ignore')

            if text == "*1*":
                self.app.last_connection_time = datetime.now()
                return 
            
            print(text)
            nickname = ''
            msg = ''
            for num, i in enumerate(text.split(':'), start=0):
                if num == 0:
                    nickname = i
                elif num == 1:
                    msg += i
                else:
                    msg += ':' + i

            print("Decryption: {}".format(self.app.V.decryptMsg(msg)))
            
            self.app.root.ids.chat_logs.text += (
            '[b][color=2980b9]{}:[/color][/b] {}\n'.format(nickname, esc_markup(self.app.V.decryptMsg(msg)))
            #'[b][color=2980b9]{}:[/color][/b] {}\n'.format(nickname, esc_markup(msg))
            )

            Clipboard.copy(msg)

    def connection_lost(self, exc):
        print('The server closed the connection')
        self.transport.close()


class RootWidget(ScreenManager):
    def __init__(self, **kwargs):
        super(RootWidget, self).__init__(**kwargs)


class ChatApp(App):

    def build(self):
        self.base_folder =os.path.dirname(os.path.abspath('.'))
        self.setting_file = os.path.join(self.base_folder, 'chat_setting.json')
        self.read_config() 
        self.V = valens.Valens()
        return RootWidget()

    def read_config(self):
        try:
            with open(self.setting_file, 'r') as f:
                text = f.read()
            self.setting_dict = json.loads(text)

            self.host = self.setting_dict['host']
            self.nick = self.setting_dict['nick']
        except:
            self.host = "127.0.0.1"
            self.nick = "kivy"

    def save_config(self):
        self.setting_dict = {'host': self.host, 'nick': self.nick}
        with open(self.setting_file, 'w') as f:
            f.write(json.dumps(self.setting_dict))

    def connect(self):
        self.host = self.root.ids.server.text
        self.nick = self.root.ids.nickname.text

        self.is_stop = False
        self.loop = asyncio.get_event_loop()

        if self.reconnect():

            self.clock_receive = Clock.schedule_interval(self.receive_msg, 1)
            self.clock_detect = Clock.schedule_interval(self.detect_if_offline, 3)

            self.root.current = 'chatroom'
            self.save_config()
            print('-- connecting to ' + self.host)

    def reconnect(self):
        try:
            self.coro = self.loop.create_connection(lambda: ClientProtocol(self, self.loop),
                          self.host, PORT)
            self.transport, self.protocol = self.loop.run_until_complete(self.coro)

            self.last_connection_time = datetime.now()
            print("I just reconnected the server.")
            return True
        except Exception as e:
            #print(e)
            self.root.current = 'login'
            try:
                self.clock_receive.cancel()
                self.clock_detect.cancel()
            except:
                print("No server available.")
            return False

    def detect_if_offline(self, dt): #run every 3 seconds
        if (datetime.now() - self.last_connection_time).total_seconds() > 45:
            self.transport.close()
            self.reconnect()

    def send_msg(self):
        if self.transport.is_closing():
            self.transport.close()
            self.reconnect()
        #msg = self.root.ids.message.text
        msg = self.V.encryptMsg(self.root.ids.message.text)
        self.transport.write('{0}:{1}'.format(self.nick, msg).encode('utf-8', 'ignore'))
        self.root.ids.chat_logs.text += (
            '[b][color=2980b9]{}:[/color][/b] {}\n'
                .format(self.nick, esc_markup(self.V.decryptMsg(msg))))
        self.root.ids.message.text = ''

    def receive_msg(self, dt):
        self.loop.run_until_complete(self.coro)

    def on_stop(self):
        exit()


if __name__ == '__main__':
    ChatApp().run()

import asyncio
from datetime import datetime

from kivy.app import App
from kivy.lang import Builder
from kivy.config import Config
from kivy.uix.screenmanager import ScreenManager

from kivy.clock import Clock
from kivy.core.clipboard import Clipboard

import websocket
import threading
import os
import json

import valens

Config.set('kivy','log_level','debug')
PORT = 8000 


def esc_markup(msg):
    return (msg.replace('&', '&amp;')
            .replace('[', '&bl;')
            .replace(']', '&br;'))

class RootWidget(ScreenManager):
    def __init__(self, **kwargs):
        super(RootWidget, self).__init__(**kwargs)


class ChatApp(App):

    def build(self):
        self.base_folder =os.path.dirname(os.path.abspath('.'))
        self.setting_file = os.path.join(self.base_folder, 'chat_setting.json')
        self.read_config() 
        self.V = valens.Valens()
        return RootWidget()

    def read_config(self):
        try:
            with open(self.setting_file, 'r') as f:
                text = f.read()
            self.setting_dict = json.loads(text)

            self.host = self.setting_dict['host']
            self.username = self.setting_dict['username']
        except:
            self.host = "127.0.0.1"
            self.username = "kivy"
        
        self.password = ''

    def save_config(self):
        self.setting_dict = {'host': self.host, 'username': self.username}
        with open(self.setting_file, 'w') as f:
            f.write(json.dumps(self.setting_dict))

    def _on_open(self):
        print("Websocket opened.")
        self.root.ids.chat_logs.text += (
        'Websocket connected.\n'
        )
        #self.transport = transport
        self._join_room('test')

    def _join_room(self, room):

        self.ws.send(
            json.dumps({
                'command': 'join',
                'room': room,
            })
        )
    
    def _on_message(self, data):

        if not data:
            self._add_msg('Empty data.')
            return
        
        #text = data.decode('utf-8', 'ignore')
        text = data

        # Initialize
        print('text: {}'.format(text))
        username = ''
        msg = ''

        # Begin message handling
        # ERROR
        if hasattr(text, 'error'):
            self._add_msg('ERROR: {}'.format(text.error))
            return

        # JOIN
        if hasattr(text, 'join'):
            self._add_msg('JOIN: not implemented yet')
            return

        # LEAVE
        if hasattr(text, 'leave'):
            self._add_msg('LEAVE: not implemented yet')
            return

        # MSG
        if hasattr(text, 'message'): 
            if not hasattr(text, 'msg_type'):
                self._add_msg('Unrecognized msg type.')
                return
            
            msg_type = int(text['msg_type'])

            if msg_type == 0:
                # message
                username = 'unknown'
                if hasattr(text, 'username'):
                    username = text['username']
                self._add_msg(text['message'], username)
                return
            
            elif msg_type == 1:
                # warning /advice messages
                self._add_msg('Warning: {}'.format(text['message']))
                return

            elif msg_type == 2:
                # alert / danger messages
                self._add_msg('Alert: {}'.format(text['message']))
                return

            elif msg_type == 3:
                # muted message
                self._add_msg('Muted: {}'.format(text['message']))
                return

            elif msg_type == 4:
                # user joined room
                username = 'unknown'
                if hasattr(text, 'username'):
                    username = text['username']
                self._add_msg('User joined: {}'.format(username))
                return

            elif msg_type == 5:
                # user left room
                username = 'unknown'
                if hasattr(text, 'username'):
                    username = text['username']
                self._add_msg('User left: {}'.format(username))
                return

            else:
                self._add_msg('Unsupported message type')
                return

        self._add_msg('Unknown data: {}'.format(text))
        
        #msg = self.app.V.decryptMsg(msg)
        #print("Decryption: {}".format(msg))
            
    def _add_msg(self, msg, username=None):

        if username:
            txt = '[b][color=2980b9]{}:[/color][/b] {}\n'.format(username, esc_markup(msg))
        else:
            txt = '[b][color=3910e9]{}:[/color][/b]\n'.format(esc_markup(msg))
            
        
        self.root.ids.chat_logs.text += (
            txt
        )

        #Clipboard.copy(msg)

    def _on_close(self):
        print('The server closed the connection')
        #self.transport.close()

    def _on_error(self, error):
        print("Websocket error: {}".format(error))
    
    def connect(self):
        self.host = self.root.ids.server.text
        self.username = self.root.ids.username.text

        self.is_stop = False
        self.loop = asyncio.get_event_loop()

        #if self.reconnect():

        #self.clock_receive = Clock.schedule_interval(self.receive_msg, 1)
        #self.clock_detect = Clock.schedule_interval(self.detect_if_offline, 3)

        self.root.current = 'chatroom'
        self.save_config()
        print('-- connecting to ' + self.host)

        websocket.enableTrace(True)        
        self.ws = websocket.WebSocketApp('ws://{}:{}/chat/stream/'.format(self.host, PORT),
                                         on_open = self._on_open,
                                         on_message = self._on_message,
                                         on_close = self._on_close,
                                         on_error = self._on_error)

        #self.ws.run_forever()
        wst = threading.Thread(target=self.ws.run_forever)
        wst.daemon = True
        wst.start()

    #def reconnect(self):
    #    try:
    #        self.coro = self.loop.create_connection(lambda: ClientProtocol(self, self.loop),
    #                      self.host, PORT)
    #        self.transport, self.protocol = self.loop.run_until_complete(self.coro)

    #        self.last_connection_time = datetime.now()
    #        print("I just reconnected the server.")
    #        return True
    #    except Exception as e:
    #        #print(e)
    #        self.root.current = 'login'
    #        try:
    #            self.clock_receive.cancel()
    #            self.clock_detect.cancel()
    #        except:
    #            print("No server available.")
    #        return False

    #def detect_if_offline(self, dt): #run every 3 seconds
    #    if (datetime.now() - self.last_connection_time).total_seconds() > 45:
    #        self.transport.close()
    #        self.reconnect()

    def send_msg(self):
        #if self.transport.is_closing():
        #    self.transport.close()
        #    self.reconnect()
        
        msg = self.root.ids.message.text
        emsg = self.V.encryptMsg(self.root.ids.message.text)
        
        #self.transport.write('{0}:{1}'.format(self.nick, msg).encode('utf-8', 'ignore'))
        #self.ws.send('{0}:{1}'.format(self.nick, msg))
        
        self.ws.send(
            json.dumps({
                'command': 'send',
                'room': 'test',
                'message': msg,
            })
        )

        #self.root.ids.chat_logs.text += (
        #    '[b][color=2980b9]{}:[/color][/b] {}\n'
        #        .format(self.nick, esc_markup(msg)))
        
        self._add_msg(msg, self.username)
        self.root.ids.message.text = ''

    #def receive_msg(self, dt):
    #    self.loop.run_until_complete(self.coro)

    def on_stop(self):
        exit()


if __name__ == '__main__':
    ChatApp().run()
